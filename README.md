# Coraline Data Science Project

## Prerequisites
[![Python 3.6.9](https://img.shields.io/badge/python-3.6.9-blue.svg)](https://www.python.org/downloads/release/python-369/)

## Instruction for Docker use
1.  On CLI, Go to the root project folder
2.  Copy .env_example to .env
    ```
    $ cp .env_example .env
    ```
3.  Edit your configuration and credentials in `.env`
4.  Run docker compose with build
    ```
    $ docker-compose up
    ```
    Note:
    - add `-d` for detach session (run in background)
    - add `--build` if build is required
5.  Enjoy jupyter notebook service via [localhost:8888](http://localhost:8888) on browser
6.  When finished using service, shut down by the following
    - If running `docker-compose` with `-d`, run
        ```
        $ docker-compose down
        ```
    - Otherwise, `Ctrl+C` twice on terminal
7.  If want to run an individual python script, run the following line
    ```
    $ docker-compose run python path/to/python/script
    ```
    e.g.
    ```
    $ docker-compose run python src/project/example_module/example.py
    ```



## Installation Guide (Manual)

### Setup Virtual Environment
1.  On CLI, Go to the root project folder
2.  Create a virtual environment
    ```
    $ python -m venv venv
    ```
3.  Activate the virtual environment
    -   In Windows OS,
        ```
        $ source venv/Scripts/activate
        ```
    -   In Linux OS,
        ```
        $ source venv/bin/activate
        ```
3.  Upgrade PIP
    ```
    (venv) $ python -m pip install --upgrade pip
    ```
4.  Install prerequisite libraries
    ```
    (venv) $ pip install -r requirements.txt
    ```


### Setup Environment Variables
1.  On CLI, Go to the root project folder
2.  Copy .env_example to .env
    ```
    $ cp .env_example .env
    ```
3.  Edit your configuration and credentials in `.env`
4.  Activate the virtual environment (see _Setup virtual environment_)
5.  Export .env
    ```
    (venv) $ export $(cat .env | sed -e /^$/d -e /^#/d | xargs)
    ```


### [Optional] Setup Jupyter Notebook and Extension
1.  On CLI, Go to the root project folder
2.  Install jupyter notebook and extension package

    (This should be done at step _Install prerequisite libraries_ in _Setup Virtual Environment_)
    ```
    (venv) $ pip install jupyter jupyter_contrib_nbextensions
    ```
3.  Activate nbextension
    ```
    (venv) $ jupyter contrib nbextension install --user;
    ```
4.  Enable helpful extension
    ```
    (venv) $ jupyter nbextension enable nbextensions_configurator/config_menu/main;jupyter nbextension enable contrib_nbextensions_help_item/main;jupyter nbextension enable toggle_all_line_numbers/main;jupyter nbextension enable varInspector/main;jupyter nbextension enable toc2/main;jupyter nbextension enable codefolding/main;jupyter nbextension enable collapsible_headings/main;jupyter nbextension enable autosavetime/main;jupyter nbextension enable execute_time/ExecuteTime;jupyter nbextension enable skip-traceback/main;jupyter nbextension enable nbextensions_configurator/tree_tab/main;jupyter nbextension enable codefolding/edit;
    ```
5. Enjoy your work with extension on Jupyter Notebook
    ```
    (venv) $ jupyter notebook
    ```


### [Optional] Integrate Jupyter Notebook on VSCode
1.  Setup virtual environment
2.  Run the following line to install kernel
    ```
    $ ipython kernel install --user --name=venv
    ```
3.  Open project folder on VSCode and open ipynb file
4.  Click "**Select Kernel**" on the top-right menu
5.  Choose the one with **venv**
6.  Enjoy your work with VSCode
