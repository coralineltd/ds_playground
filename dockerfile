FROM python:3.6.9 AS python_base

ARG APP_FOLDER=/app

RUN mkdir -p ${APP_FOLDER}
COPY requirements.txt ${APP_FOLDER}/.
WORKDIR ${APP_FOLDER}

RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

COPY src/project src/project
COPY setup.py setup.py
RUN pip install --editable .

FROM python_base AS jupyter_notebook

# Install jupyter notebook extension
RUN jupyter contrib nbextension install --user;\
    jupyter nbextension enable nbextensions_configurator/config_menu/main;\
    jupyter nbextension enable contrib_nbextensions_help_item/main;\
    jupyter nbextension enable toggle_all_line_numbers/main;\
    jupyter nbextension enable varInspector/main;\
    jupyter nbextension enable toc2/main;\
    jupyter nbextension enable codefolding/main;\
    jupyter nbextension enable collapsible_headings/main;\
    jupyter nbextension enable autosavetime/main;\
    jupyter nbextension enable execute_time/ExecuteTime;\
    jupyter nbextension enable skip-traceback/main;\
    jupyter nbextension enable nbextensions_configurator/tree_tab/main;\
    jupyter nbextension enable codefolding/edit;

#Configure container to support easier access to jupyter notebook
ARG JPYNB_TOKEN=-1
RUN mkdir -p $HOME/.jupyter/
RUN if [ $JPYNB_TOKEN!=-1 ]; then echo "c.NotebookApp.token='$JPYNB_TOKEN'" >> $HOME/.jupyter/jupyter_notebook_config.py; fi

CMD bash

# docker run --env-file=.env --rm -p 8888:8888 ds_playground
# docker-compose up