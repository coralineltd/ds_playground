from coralinedb import MySQLDB
from sshtunnel import SSHTunnelForwarder
import os

from project import get_logger
from logging import Logger

logger = get_logger("db_manager", 'WARNING')

# Load Environment variable
DB_HOST = os.environ["DB_HOST"]
DB_USER = os.environ["DB_USER"]
DB_PASSWORD = os.environ["DB_PASSWORD"]

# SSH Credentials
ENABLE_SSH_TUNNEL = int(os.getenv("ENABLE_SSH_TUNNEL", 0))
SSH_HOST = os.getenv("SSH_HOST")
SSH_PORT = os.getenv("SSH_PORT")
SSH_USER = os.getenv("SSH_USER")
SSH_KEY_PATH = os.getenv("SSH_KEY_PATH")

def set_ssh_tunnel_forwarder(
        ssh_host: str,
        ssh_port: str,
        ssh_user: str,
        ssh_key_path: str,
        remote_host: str,
        remote_port: str,
        logger: Logger = logger,
        local_host: str = '127.0.0.1',
        local_port: str = '3306'):
    logger.debug(f'Starting SSH tunnel forward..')
    tunnel = SSHTunnelForwarder(
        (ssh_host, int(ssh_port)),
        ssh_username=ssh_user,
        ssh_private_key=ssh_key_path,
        remote_bind_address=(remote_host, int(remote_port)),
        local_bind_address=(local_host, int(local_port)),
        logger=logger)

    tunnel.daemon_forward_servers = True

    return tunnel, local_host, local_port


def ssh_wrapper(func):
    def connect_and_call(obj, *args, **kwargs):

        # Start tunnel
        if obj.use_ssh_tunnel and not obj.ssh_tunnel.is_active:
            obj.ssh_tunnel.start()

        # Call original function
        res = func(obj, *args, **kwargs)

        # Stop tunnel
        if obj.use_ssh_tunnel and obj.ssh_tunnel.is_active:
            obj.ssh_tunnel.stop()

        return res

    return connect_and_call


class ProjectDB(MySQLDB):

    def __init__(self):
        super().__init__(DB_HOST, DB_USER, DB_PASSWORD)

        # SSH
        self.use_ssh_tunnel = False
        self.ssh_tunnel = None
        self.logger = logger
        if ENABLE_SSH_TUNNEL:
            self.use_ssh_tunnel = True
            tunnel, local_host, local_port = set_ssh_tunnel_forwarder(
                ssh_host=SSH_HOST,
                ssh_port=SSH_PORT,
                ssh_user=SSH_USER,
                ssh_key_path=SSH_KEY_PATH,
                remote_host=self.host,
                remote_port='3306',
                logger=self.logger
            )
            self.host = f'{local_host}:{local_port}'
            self.ssh_tunnel = tunnel

    #####################################
    #                                   #
    #    Wrap all functions with SSH    #
    #                                   #
    #####################################
    @ssh_wrapper
    def load_table(self, *args, **kwargs):
        res = super().load_table(*args, **kwargs)
        return res

    @ssh_wrapper
    def load_tables(self, *args, **kwargs):
        res = super().load_tables(*args, **kwargs)
        return res

    @ssh_wrapper
    def save_table(self, *args, **kwargs):
        res = super().save_table(*args, **kwargs)
        return res

    @ssh_wrapper
    def get_databases(self, *args, **kwargs):
        res = super().get_databases(*args, **kwargs)
        return res

    @ssh_wrapper
    def get_tables(self, *args, **kwargs):
        res = super().get_tables(*args, **kwargs)
        return res

    @ssh_wrapper
    def query(self, *args, **kwargs):
        res = super().query(*args, **kwargs)
        return res

    @ssh_wrapper
    def get_count(self, *args, **kwargs):
        res = super().get_count(*args, **kwargs)
        return res

    @ssh_wrapper
    def execute(self, *args, **kwargs):
        res = super().execute(*args, **kwargs)
        return res


if __name__ == "__main__":
    logger.info('START !!!')
    db = ProjectDB()
    branch = db.load_table('ri_integration', "branch")
    logger.info(branch.head())
    logger.info('DONE !!!')
