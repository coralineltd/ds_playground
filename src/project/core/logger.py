import os
import sys
import logging

LOGLEVEL = os.environ.get('LOGLEVEL', 'INFO').upper()

def get_logger(process_name, loglevel=LOGLEVEL):
    """
    Create logger with process_name
    :param process_name: str
    :return:
    """
    # create logger
    logger = logging.getLogger(process_name)
    logger.setLevel(logging.getLevelName(loglevel))

    # create console handler and set level to debug
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s %(levelname)s: %(name)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    # logger.addHandler(ch)
    logger.handlers = [ch]

    return logger


if __name__ == '__main__':
    logger = get_logger("TEST")
    logger.debug('debug message')
    logger.info('info message')
    logger.warning('warn message')
    logger.error('error message')
    logger.critical('critical message')
