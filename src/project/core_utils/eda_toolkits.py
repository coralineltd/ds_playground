import numpy as np
import pandas as pd
import warnings

from scipy.stats import pearsonr, spearmanr, kendalltau
import matplotlib.pyplot as plt
import seaborn as sns

#---------------------------------------------------------
#---------------          Outlier          ---------------
#---------------------------------------------------------
def get_lower_bound_by_iqr(data):
    Q1, Q3 = np.percentile(data, [25, 75])
    IQR = Q3-Q1
    lower_bound = Q1-(1.5*IQR)
    return lower_bound


def get_upper_bound_by_iqr(data):
    Q1, Q3 = np.percentile(data, [25, 75])
    IQR = Q3-Q1
    upper_bound = Q3+(1.5*IQR)
    return upper_bound


def get_lower_upper_bound_by_iqr(data):
    Q1, Q3 = np.percentile(data, [25, 75])
    IQR = Q3-Q1
    lower_bound = Q1-(1.5*IQR)
    upper_bound = Q3+(1.5*IQR)
    return (lower_bound, upper_bound)


def drop_outlier_by_iqr(df, columns:list=None):
    df = df.copy()
    if columns is None:
        columns = df.columns.tolist()

    for col in columns:
        lower_bound, upper_bound = get_lower_upper_bound_by_iqr(df[col])

        df = df[(df[col] >= lower_bound)&(df[col] <= upper_bound)]
    return df


#---------------------------------------------------------
#---------------           ECDF            ---------------
#---------------------------------------------------------
def ecdf(data):
    return sorted(data), np.arange(1,len(data)+1)/len(data)


def plot_ecdf(data, xlim=None, ylim=None):
    x, y = ecdf(list(data))
    plt.figure(figsize=(16,6))
    plt.plot(x, y, marker='.', linestyle='none')
    plt.grid()
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.xlabel('value')
    plt.ylabel(f'% data ({len(data):,} in total)')
    plt.show()


#---------------------------------------------------------
#---------------        Correlation        ---------------
#---------------------------------------------------------
def corr_with_pvalue(df, method='pearson', row_cols=None, column_cols=None):
    df = df.dropna(axis=1, how='all')._get_numeric_data()
    dfcols = pd.DataFrame(columns=df.columns, dtype='float')

    row_cols = df.columns if row_cols is None else row_cols
    column_cols = df.columns if column_cols is None else column_cols

    corr = dfcols.transpose().join(dfcols, how='outer').loc[row_cols, column_cols]
    pvalues = dfcols.transpose().join(dfcols, how='outer').loc[row_cols, column_cols]

    # Specify correlation method
    corr_fn = pearsonr
    if method == 'spearman':
        corr_fn = spearmanr
    elif method == 'kendal':
        corr_fn = kendalltau

    for r in row_cols:
        for c in column_cols:
            mask = df[r].notnull() & df[c].notnull()
            result = corr_fn(df[mask][r], df[mask][c])
            corr.loc[r, c] = round(result[0], 2)
            pvalues.loc[r, c] = round(result[1], 4)
    return corr, pvalues


def plot_corr(
    df,
    method='pearson',
    title='',
    figsize=None,
    corr_threshold=None,
    p_threshold=None,
    return_results=False):

    ncolumns = len(df.columns)

    # Compute the correlation matrix
    corr, pval = corr_with_pvalue(df, method)

    annotate = corr.astype(str)
    annotate[pval < 0.1] = annotate[pval < 0.1] + '*'
    annotate[pval < 0.05] = annotate[pval < 0.05] + '*'
    annotate[pval < 0.01] = annotate[pval < 0.01] + '*'

    # Generate a mask for the upper triangle
    mask = np.zeros_like(corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True

    if corr_threshold:
        mask = np.logical_or((corr.abs() < corr_threshold).to_numpy(),mask)

    if p_threshold:
        mask = np.logical_or((pval < p_threshold).to_numpy(),mask)

    # Set up the matplotlib figure
    if figsize is None:
        fsize = int(ncolumns*.75)
        figsize = (fsize, fsize)
    plt.figure(figsize=figsize)

    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(
        corr,
        mask=mask,
        cmap='bwr',
        vmin=-1,
        vmax=1,
        center=0,
        square=True,
        linewidths=.5,
        cbar_kws={"shrink": .5},
        annot=annotate,
        fmt='')

    plt.title(title)

    if return_results:
        return corr, pval


def plot_missing(data, figsize=(20, 5)):
    ax = data.isna().sum().sort_values(ascending=False).plot.bar(
        figsize=figsize,
        xlabel='column names',
        ylabel='count',
    )
    for p in ax.patches:
        ax.annotate(
            f'{p.get_height()/data.shape[0]*100:.2f} %',
            (p.get_x(),
            p.get_height() + ax.get_ylim()[1] * 0.02),
            rotation=90
        )
    plt.show()


def plot_asso_rule_stats(data: pd.DataFrame, rule_index: pd.Series):
    col = 'age_range'
    tmp = data[rule_index][col]
    cnt = tmp.value_counts().rename('cnt')

    print(f'% rule matched: {tmp.shape[0]} / {data.shape[0]} = {(tmp.shape[0] / data.shape[0] * 100):.6f} %')

    res = cnt.to_frame().assign(
        pct_confidence=cnt/tmp.shape[0]*100,
        pct_support=cnt/data.shape[0]*100)

    ax = cnt.plot.bar()

    for p in ax.patches:
        ax.annotate(
            f'{p.get_height()/tmp.shape[0]*100:.2f} %',
            (p.get_x(),
            p.get_height() + ax.get_ylim()[1] * 0.02),
            rotation=90
        )

    plt.title(f'{col}')
    plt.show()


def boxplot_with_n_obs(
    data: pd.DataFrame,
    x: str,
    y: str,
    title=None,
    figsize=(20,8),
):

    with warnings.catch_warnings():
        # this will suppress all warnings in this block
        warnings.simplefilter("ignore")

        val_counts = data[x].astype(str).value_counts().rename('cnt')

        plt.figure(figsize=figsize)
        ax = sns.boxplot(data=data.sort_values(x), x=x, y=y)

        if len(ax.get_xticklabels()) > 1:
            ax2 = plt.twinx()
            sns.lineplot(
                data=val_counts.loc[[l.get_text() for l in ax.get_xticklabels()]],
                ax=ax2,
                sort=False)

        ax.set_xticklabels(
            # [f'{l.get_text()} ({val_counts.loc[l.get_text()]:,})'
            #     for l in ax.get_xticklabels()],
            ax.get_xticklabels(),
            rotation=45
        )
        plt.title(title)

        plt.show()


def get_value_counts(s: pd.Series, dropna: bool = False, bins=None):
    return pd.concat([
        s.value_counts(dropna=dropna, bins=bins).rename('cnt'),
        100*s.value_counts(dropna=dropna, normalize=True, bins=bins).rename('%'),
    ], axis=1)


#---------------------------------------------------------
#---------------       Time series         ---------------
#---------------------------------------------------------
